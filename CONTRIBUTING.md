# Contributing

Feel free to [report bugs](https://gitlab.com/pymarc/pymarc/-/issues) you have encountered
and [suggest new features](https://gitlab.com/pymarc/pymarc/-/merge_requests).

For any new development, please respect the standards in place on the project (enforced
by the CI):

* formatting with [black](https://github.com/psf/black)
* validated by [flake8](http://flake8.pycqa.org/en/latest) 
  and (coming soon) [mypy](http://mypy-lang.org)
* tested with [unittest](https://docs.python.org/fr/3/library/unittest.html)
* compatible from python 3.3 to python 3.8

To install development dependencies: `pip install -r requirements.dev.txt`.
